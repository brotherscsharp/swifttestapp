//
//  LoginResponce.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 04.03.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
struct LoginResponce {
    let idToken: String
}

extension LoginResponce: Codable{
    enum LoginResponceKeys: String, CodingKey {
        case idToken = "id_token"
    }
    
    init(from decoder: Decoder) throws {
        let transactionContainer = try decoder.container(keyedBy: LoginResponceKeys.self)
        idToken = try transactionContainer.decode(String.self, forKey: .idToken)
    }
}
