//
//  User.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

class User: Decodable {
    var id: Int
    var name: String
    var email: String
    var balance: Float
}
