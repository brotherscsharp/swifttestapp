//
//  UserRecipient.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 27.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
class UserRecipient: Decodable {
    var id: Int
    var name: String
}
