//
//  Transaction.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

class Transaction: Decodable {
    var id: Int
    var date: String
    var username: String
    var amount: Float
    var balance: Float
}

struct Transactions {
    let transToken: [Transaction]
}

extension Transactions: Decodable{
    enum TransactionsKeys: String, CodingKey {
        case transToken = "trans_token"
    }
    
    init(from decoder: Decoder) throws {
        let transactionContainer = try decoder.container(keyedBy: TransactionsKeys.self)
        transToken = try transactionContainer.decode([Transaction].self, forKey: .transToken)
    }
}

struct TransactionResult {
    let transToken: Transaction
}

extension TransactionResult: Decodable{
    
    enum TransactionResultKeys: String, CodingKey {
        case transToken = "trans_token"
    }
    
    init(from decoder: Decoder) throws {
        let transactionContainer = try decoder.container(keyedBy: TransactionResultKeys.self)
        transToken = try transactionContainer.decode(Transaction.self, forKey: .transToken)
    }
}
