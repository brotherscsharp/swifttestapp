//
//  NetworkUser.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 26.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

struct NetworkUser {
    let userInfoToken: User
}

extension NetworkUser: Decodable{
    enum NetworkUserKeys: String, CodingKey {
        case userInfoToken = "user_info_token"
    }
    
    init(from decoder: Decoder) throws {
        let transactionContainer = try decoder.container(keyedBy: NetworkUserKeys.self)
        userInfoToken = try transactionContainer.decode(User.self, forKey: .userInfoToken)
    }
}
