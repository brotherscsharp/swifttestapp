//
//  SharedApi.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 25.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

class Api {
    
    static var baseUrl: String = "http://193.124.114.46:3001"
    
    // endpoins
    static var register: String = "/users"
    static var login: String = "/sessions/create"
    static var userTransactionsList: String = "/api/protected/transactions"
    static var sendFunds: String = "/api/protected/transactions"
    static var userInfo: String = "/api/protected/user-info"
    static var filterUsers: String = "/api/protected/users/list"
}
