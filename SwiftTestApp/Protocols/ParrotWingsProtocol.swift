//
//  ParrotWingsProtocol.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

protocol ParrotWingsProtocol {
    static func sendFunds(recipientName: String, amount: Float, result:@escaping (_ success:Bool,_ error:String?)->Void)
    static func getTransactions(result:@escaping (_ success:Bool,_ error:String?,_ transactions:[Transaction]?)->Void)
}
