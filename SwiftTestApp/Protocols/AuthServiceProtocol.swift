//
//  AuthServiceProtocol.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
protocol AuthServiceProtocol {
    static func login(email: String, password: String, result:@escaping (_ success:Bool,_ error:String?)->Void)
    static func register(userName: String, password: String, email: String, result:@escaping (_ success:Bool,_ error:String?)->Void)
}
