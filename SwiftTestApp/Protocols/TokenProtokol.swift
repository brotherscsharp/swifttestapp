//
//  TokenProtokol.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
protocol TokenProtocol {
    static var token: String { get set }
}
