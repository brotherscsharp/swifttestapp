//
//  UsersProtocol.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
protocol UsersProtocol {
    static func getUserInfo(result:@escaping (_ success:Bool,_ error:String?,_ userInfo:User?)->Void)
    static func getUsersByName(filter: String, result:@escaping (_ success:Bool,_ error:String?, _ userInfo:[UserRecipient]?)->Void)
}
