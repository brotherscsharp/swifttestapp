//
//  TransactionTableViewCell.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 26.03.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIndicator: UIImageView!
    @IBOutlet weak var lbUserName: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbOutgoingAmount: UILabel!
    @IBOutlet weak var lbBalance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
