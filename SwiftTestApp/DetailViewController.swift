//
//  DetailViewController.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var txtNickName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lbOrRegister: UIButton!
    
    var isRegistration: Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // test credentials
        txtEmail.text = "servin1@sys.im"
        txtPassword.text = "1234qwer"
    }
    
    fileprivate func goBack() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func LoginOrRegisterClick(_ sender: UIButton) {
        if validateForm() {
            if isRegistration {
                register()
            } else {
                login()
            }
        }
    }
    
    func validateForm() -> Bool {
        if isRegistration {
            if txtNickName?.text?.count == 0 {
                showDialog(error: NSLocalizedString("Please type nick name", comment: ""))
                return false
            }
        }
        
        if txtPassword?.text?.count == 0 {
            showDialog(error: NSLocalizedString("Please type password", comment: ""))
            return false
        }
        
        if txtEmail?.text?.count == 0 {
            showDialog(error: NSLocalizedString("Please type email", comment: ""))
            return false
        }
        return true
    }

    func login() {
        AuthService.login(email: txtEmail.text!, password: txtPassword.text!){ (success, error) in
            if success {
                self.goBack()
            } else {
             self.showDialog(error: error!)
            }
        }
    }

    func register() {
        AuthService.register(userName: txtNickName.text!, password: txtPassword.text!, email: txtEmail.text!){ (success, error) in
            if success {
                self.goBack()
            } else {
                self.showDialog(error: error!)
            }
        }
    }

    func showDialog(error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: NSLocalizedString("Alert", comment: ""), message: error, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func SwitchToRegister(_ sender: Any) {
        if isRegistration {
            isRegistration = false
            lbTitle.text = NSLocalizedString("Login", comment: "")
            txtNickName.isHidden = true
            btnLogin.setTitle(NSLocalizedString("Login", comment: ""), for: UIControl.State.normal)
            lbOrRegister.setTitle(NSLocalizedString("or Register", comment: ""), for: UIControl.State.normal)
        } else {
            isRegistration = true
            lbTitle.text = NSLocalizedString("Sign Up", comment: "")
            txtNickName.isHidden = false
            btnLogin.setTitle(NSLocalizedString("Sign Up", comment: ""), for: UIControl.State.normal)
            lbOrRegister.setTitle(NSLocalizedString("or Login", comment: ""), for: UIControl.State.normal)
        }
    }
}

