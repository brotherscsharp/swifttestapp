//
//  Model.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 04.03.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
struct User1: Codable {
    let id: Int
}

struct Repository: Codable {
    let id: Int
    let name: String
    let description: String?
}

struct Issue: Codable {
    let id: Int
}
