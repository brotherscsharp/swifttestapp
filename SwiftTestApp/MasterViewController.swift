//
//  MasterViewController.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: SendFundsViewController? = nil
    var transactionList = [Transaction]()
    
    //@IBOutlet weak var lbUserName: UILabel!
    @IBOutlet weak var lbBalance: UILabel!
    @IBOutlet weak var btnSendFunds: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Login", comment: ""), style: .plain, target: self, action: #selector(loginTapped))
       /*
         
        let me = "V8tr"
       let repos = GithubAPI.repos(username: me)
       let firstRepo = repos.compactMap { $0.first }
       let issues = firstRepo.flatMap { repo in
           GithubAPI.issues(repo: repo.name, owner: me)
       }
       
       let token = issues.sink(receiveCompletion: { _ in },
                               receiveValue: { print($0) })
       
       RunLoop.main.run(until: Date(timeIntervalSinceNow: 10))

       withExtendedLifetime(token, {})*/
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        btnSendFunds.setTitleColor(UIColor.darkGray, for: UIControl.State.disabled)
        getUserInfo()
    }
    
    @IBAction func sendFundsClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SendFundsViewController") as! SendFundsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func updateUserInfo(_ sender: Any) {
        getUserInfo()
    }
    
    func getUserInfo() {
        if !(TokenService.token).isEmpty {
            self.navigationItem.rightBarButtonItem = nil;
            UserService.getUserInfo() { (success, error, userInfo) in
                if success {
                    DispatchQueue.main.async {
                        self.title = userInfo?.name
                        //self.lbUserName.text = userInfo?.name
                        self.lbBalance.text = "\(userInfo?.balance ?? 0) PW"
                        self.btnSendFunds.isEnabled = true
                        self.btnSendFunds.isHidden = false
                        self.lbBalance.isHidden = false
                    }
                }
            }
            
            ParrotWingsService.getTransactions() { (success, error, userInfo) in
                if success {
                    DispatchQueue.main.async {
                        self.transactionList = userInfo!
                        self.tableView.reloadData()
                    }
                }
            }
        } else {
            self.openLoginForm()
        }
    }
    
    @objc
    func loginTapped(_ sender: Any) {
        openLoginForm()
    }
    
    func openLoginForm() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! DetailViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
        
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = transactionList[indexPath.row]
                 let controller = (segue.destination as! UINavigationController).topViewController as! SendFundsViewController
                controller.selectedTransaction = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                detailViewController = controller
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! TransactionTableViewCell
        let object = transactionList[indexPath.row]
        
        cell.lbUserName.text = object.username
        cell.lbDate.text = object.date
        cell.lbOutgoingAmount.text = "\(object.amount)PW"
        cell.lbBalance.text = "\(object.balance)PW"
        cell.imgIndicator.image = UIImage(named: object.amount > 0 ? "in": "out")!
        //cell.textLabel!.text = "-> \(object.username): \(object.amount)PW  \(object.date)"
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            transactionList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

