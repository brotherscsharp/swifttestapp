//
//  RemoteService.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
import Combine

class RemoteService {
    static var methodPost = "POST"
    static var methodGet = "GET"
    
    static func login(email: String, password: String, result:@escaping (_ success:Bool,_ error:String?, _ _data:[String:Any?]?)->Void) {
        let parameters = ["email": email, "password" : password]
                
        postRequest(endpoint: Api.login, parameters: parameters, method: methodPost, isNeedToken: false) { (success, error, data) in
            do {
                if success {
                    let resultsDic = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                    result(success, error, resultsDic)
                } else {
                    result(success, (error as! String), nil)
                }
            } catch {
                result(success, (error as! String), nil)
            }
        }
    }
    
    static func register(userName: String, email: String, password: String, result:@escaping (_ success:Bool,_ error:String?, _ _data:[String:Any?]?)->Void) {
        let parameters = ["email": email, "password": password, "username": userName]
        postRequest(endpoint: Api.register, parameters: parameters, method: methodPost, isNeedToken: false) { (success, error, data) in
            do {
                if success {
                    let resultsDic = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                    result(success, error, resultsDic)
                } else {
                    result(success, (error as! String), nil)
                }
            } catch {
                result(success, (error as! String), nil)
            }
        }
    }
    
    static func getUserTransactionsList(result:@escaping (_ success:Bool,_ error:String?, _ _data:[Transaction]?)->Void) {
        postRequest(endpoint: Api.userTransactionsList, parameters: [], method: methodGet, isNeedToken: true) { (success, error, data) in
            do {
                if success {
                    let resultsDic = try JSONDecoder().decode(Transactions.self, from: data!)
                    result(success, error, resultsDic.transToken)
                } else {
                    result(success, (error as! String), nil)
                }
            } catch {
                result(success, (error as! String), nil)
            }
        }
    }
    static func getUserInfo(result:@escaping (_ success:Bool,_ error:String?, _ _data:User?)->Void) {
        postRequest(endpoint: Api.userInfo, parameters: [], method: methodGet, isNeedToken: true) { (success, error, data) in
            
            do {
                if success {
                    let resultsDic = try JSONDecoder().decode(NetworkUser.self, from: data!)
                    result(success, error, resultsDic.userInfoToken)
                } else {
                    result(success, (error as! String), nil)
                }
            } catch {
                result(success, (error as! String), nil)
            }
        }
    }
    
    static func getUsersByName(filter: String, result:@escaping (_ success:Bool,_ error:String?, _ _data:[UserRecipient]?)->Void) {
        let parameters = ["filter": filter]
        postRequest(endpoint: Api.filterUsers, parameters: parameters, method: methodPost, isNeedToken: true) { (success, error, data) in
            do {
                if success {
                    let resultsDic = try JSONDecoder().decode([UserRecipient].self, from: data!)
                    result(success, error, resultsDic)
                } else {
                    result(success, (error as! String), nil)
                }
            } catch {
                result(success, (error as! String), nil)
            }
        }
    }
    
    static func sendFunds(recipientName: String, amount: Float, result:@escaping (_ success:Bool,_ error:String?, _ _data:Transaction?)->Void) {
        let parameters = ["name": recipientName, "amount" : amount] as [String : Any]
        postRequest(endpoint: Api.sendFunds, parameters: parameters, method: methodPost, isNeedToken: true) { (success, error, data) in
            do {
                if success {
                    let resultsDic = try JSONDecoder().decode(TransactionResult.self, from: data!)
                    result(success, error, resultsDic.transToken)
                } else {
                    result(success, (error as! String), nil)
                }
            } catch {
                result(success, (error as! String), nil)
            }
        }
    }
    
    static func postRequest(endpoint: String, parameters: Any, method: String, isNeedToken: Bool, completion:@escaping (_ success:Bool,_ error:String?, _ _data:Data?)->Void) -> URLSessionDataTask? {
        guard let url = URL(string: "\(Api.baseUrl)\(endpoint)")
            else {
                completion(false, nil, nil)
                return nil
            }
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        if isNeedToken {
            request.setValue("Bearer \(TokenService.token)", forHTTPHeaderField: "Authorization")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if method == methodPost {
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {completion(false, nil, nil); return nil }
            request.httpBody = httpBody
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
                        
            do {
                /*
                It is bad practice to use strings to define problem. But currently I did not use codes, because I did non find correct algorithm to separate exceprions
                 e.g. POST /users returns 400 for different error types
                 400: A user with that email already exists
                 400: You must send username and password

                switch response?.getStatusCode() {
                case  400:
                    break
                case  401:
                    break
                case .none:
                break
                case .some(_):
                break
                    
                }*/
                
                let resultString = String(data: data, encoding: .utf8)
                switch resultString {
                case "A user with that email exists" :
                    throw CustomError.userExists(resultString!)
                case "You must send username and password":
                    throw CustomError.incorrectUserData(resultString!)
                case "Invalid email or password.":
                    throw CustomError.invalidCredentials(resultString!)
                case "UnauthorizedError":
                    throw CustomError.unauthorized(resultString!)
                case "Invalid user":
                    throw CustomError.invalidUser(resultString!)
                case "Invalid username.":
                    throw CustomError.invalidUser(resultString!)
                case "No search string":
                    throw CustomError.userNotFound(resultString!)
                case "user not found":
                    throw CustomError.notEnoughFunds(resultString!)
                case "balance exceeded":
                    throw CustomError.balanceExceeded(resultString!)
                default: break
                    
                }
                completion(true, nil, data)
            }
            catch CustomError.userExists(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.incorrectUserData(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.invalidCredentials(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.balanceExceeded(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.notEnoughFunds(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.userNotFound(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.invalidUser(let errorMessage) {
                completion(false, errorMessage, nil)
            }
            catch CustomError.unauthorized(let errorMessage) {
                completion(false, errorMessage, nil)
            } catch {
                completion(false, nil, nil)
            }
        }
        task.resume()
        return task
    }
}

extension URLResponse {
    func getStatusCode() -> Int? {
        if let httpResponse = self as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
}
struct Response<T> {
    let value: T
    let response: URLResponse
}
