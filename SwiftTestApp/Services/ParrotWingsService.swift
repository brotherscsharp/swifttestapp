//
//  ParrotWingsService.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

class ParrotWingsService: ParrotWingsProtocol {
    static func sendFunds(recipientName: String, amount: Float, result: @escaping (Bool, String?) -> Void) {
        RemoteService.sendFunds(recipientName: recipientName, amount: amount) { (success, error, data) in
            result(success, error)
        }
    }
    
    static func getTransactions(result: @escaping (Bool, String?,_ transactions:[Transaction]?) -> Void) {
        RemoteService.getUserTransactionsList() { (success, error, data) in
            result(success, error, data)
        }
    }
}
