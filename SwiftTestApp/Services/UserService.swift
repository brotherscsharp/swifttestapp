//
//  UsersService.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

class UserService: UsersProtocol {
    static func getUserInfo(result:@escaping (_ success:Bool,_ error:String?, _ userInfo:User?)->Void) {
        RemoteService.getUserInfo() { (success, error, data) in
            result(success, error, data)
        }
    }
    
    static func getUsersByName(filter: String, result: @escaping (Bool, String?, _ userInfo:[UserRecipient]?) -> Void) {
        RemoteService.getUsersByName(filter: filter) { (success, error, data) in
            result(success, error, data)
        }
    }
}
