//
//  AuthService.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 21.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation

class AuthService: AuthServiceProtocol {
    static func login(email: String, password: String, result: @escaping (Bool, String?) -> Void) {
        RemoteService.login(email: email, password: password) { (success, error, data) in
            if success {
               TokenService.token = data?["id_token"] as! String
               result(true, nil)
            } else {
               result(false, error)
            }
        }
    }
    
    static func register(userName: String, password: String, email: String, result:@escaping (_ success:Bool,_ error:String?)->Void) {
        RemoteService.register(userName: userName, email: email, password: password) { (success, error, data) in
            if success {
                TokenService.token = data?["id_token"] as! String
                result(true, nil)
            } else {
                result(false, error)
            }
        }
    }
}
