//
//  SendFundsViewController.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 27.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//


import UIKit
import AutoCompleteTextField

class SendFundsViewController: UIViewController, ACTFDataSource, UITextFieldDelegate {
    
    //@IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtUserName: AutoCompleteTextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lbBalance: UILabel!
    
    var originalUserssList: [ACTFDomain] = []
    
    var selectedTransaction: Transaction? {
        didSet {
            initForm()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initForm()
        
        initAutocompleteField()
    }
    
    func initAutocompleteField() {
        txtUserName.addTarget(self, action: #selector(searchName(_ :)), for: .editingChanged)
        txtUserName.dataSource = self
        txtUserName.delegate = self
        txtUserName.showAutoCompleteButtonWithImage(UIImage(named: "checked"), viewMode: .whileEditing)
    }
    
    func initForm() {
        if let detail = selectedTransaction {
            if let textView = txtUserName {
                textView.text = detail.username
            }
            
            if let textView = txtAmount {
                textView.text = "\(abs(detail.amount))"
            }
        }
        
        updateBalance()
    }
    
    func updateBalance() {
        if !(TokenService.token).isEmpty {
            self.navigationItem.rightBarButtonItem = nil;
            UserService.getUserInfo() { (success, error, userInfo) in
                if success {
                    DispatchQueue.main.async {
                        self.lbBalance.text = "\(userInfo?.balance ?? 0) PW"
                    }
                }
            }
        }
    }
    
    @objc func searchName(_ textField: UITextField) {
        if textField.text!.count > 1 {
            UserService.getUsersByName(filter: textField.text!) { (success, error, data) in
                if success {
                    DispatchQueue.main.async {
                        if (data != nil && data!.count > 0) {
                            // init autocomplete sourse
                            data!.forEach {user in
                                let newUser = ACTFDomain(text: user.name, weight: 1)
                                self.originalUserssList.append(newUser)
                            }
                        } else {
                            self.originalUserssList = []
                        }
                    }
                }
            }
        }
    }
        
    @IBAction func sendFundsClicked(_ sender: Any) {
        if validateForm() {
            ParrotWingsService.sendFunds(recipientName: txtUserName.text!, amount: Float(txtAmount.text!)!) { (success, error) in
                if success {
                    self.showDialog(message: NSLocalizedString("Money was sent successfully!", comment: ""))
                    
                } else {
                    self.showDialog(message: error!);
                }
            }
        }
    }
    
    func validateForm() -> Bool {
        if txtUserName?.text?.count == 0 {
            showDialog(message: NSLocalizedString("Please type user name", comment: ""))
            return false
        }
        
        if txtAmount?.text?.count == 0 {
            showDialog(message: NSLocalizedString("Please type amount", comment: ""))
            return false
        }
        return true
    }
    
    fileprivate func goBack() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)            
        }
    }
    
    func showDialog(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: NSLocalizedString("Alert", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (_) in
                self.updateBalance()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ACTFDataSource
       
   func autoCompleteTextFieldDataSource(_ autoCompleteTextField: AutoCompleteTextField) -> [ACTFDomain] {
       return originalUserssList
   }
}

