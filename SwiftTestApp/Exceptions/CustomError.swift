//
//  BaseError.swift
//  SwiftTestApp
//
//  Created by Servin Osmanov on 26.02.2020.
//  Copyright © 2020 Servin Osmanov. All rights reserved.
//

import Foundation
enum CustomError: Error {
    case userExists(String)
    case invalidCredentials(String)
    case incorrectUserData(String)
    case unauthorized(String)
    case invalidUser(String)
    case userNotFound(String)
    case notEnoughFunds(String)
    case balanceExceeded(String)
}
